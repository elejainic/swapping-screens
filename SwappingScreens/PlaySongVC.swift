//
//  PlaySongVC.swift
//  SwappingScreens
//
//  Created by macbook pro on 24/3/18.
//  Copyright © 2018 free developer hack. All rights reserved.
//

import UIKit

class PlaySongVC: UIViewController {

    @IBOutlet weak var songTitleLb1: UILabel!
    
    private var _selectedSong:String!
    
    var selectedSong : String {
        get {
            return _selectedSong
        } set {
            _selectedSong = newValue
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

       songTitleLb1.text = _selectedSong
    }

  

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
