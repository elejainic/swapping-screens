//
//  MusicListVCViewController.swift
//  SwappingScreens
//
//  Created by macbook pro on 24/3/18.
//  Copyright © 2018 free developer hack. All rights reserved.
//

import UIKit

class MusicListVCViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.blue
    }

    
    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func load3SongPressed(_ sender: Any) {
        let songTitle = "Forever young"
        performSegue(withIdentifier: "PlaySongVC", sender: songTitle)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? PlaySongVC{
            
            if let song = sender as? String {
            destination.selectedSong = song
            }
        }
    }

}
